var formObj = document.forms.carForm;
function addRow() {
    for(var i=0, j=1;i<100;i++,j++) {
        if(document.getElementById("row"+i).hasChildNodes() == false) {
            var element = document.getElementById("row"+i);
            element.innerHTML = '<tr id="row'+i+'"><td>'+formObj.id.value+'</td><td>'+formObj.vehicleName.value+'</td><td>'+formObj.brand.value+'</td><td>'+formObj.registrationNumber.value+'</td><td>'+formObj.color.value+'</td><td>'+formObj.customerName.value+'</td><td><input class="deleteButton'+i+'" onclick="deleteRow('+i+')" value="Delete"><input type="updateButton'+i+'" onclick="updateRow('+i+')" value="Update"></td></tr><tr id="row'+j+'"></tr>';
            document.getElementById("carForm").reset();
        }
    }
}
function deleteRow(index) {
    var element = document.getElementById("row"+index);
    element.remove();
}
function updateRow(index) {
    var next = index+1;
    var element = document.getElementById("row"+index);
    element.innerHTML = '<tr id="row'+index+'"><td>'+formObj.id.value+'</td><td>'+formObj.vehicleName.value+'</td><td>'+formObj.brand.value+'</td><td>'+formObj.registrationNumber.value+'</td><td>'+formObj.color.value+'</td><td>'+formObj.customerName.value+'</td><td><input class="deleteButton'+index+'" onclick="deleteRow('+index+')" value="Delete"><input type="updateButton'+index+'" onclick="updateRow('+index+')" value="Update"></td></tr><tr id="row'+next+'"></tr>';
}
