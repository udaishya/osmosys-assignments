var str = "";
var columnCount = window.prompt("Enter column count (odd number only): ");
var rowCount = ((parseInt(columnCount) + 1) / 2);
if (columnCount%2 !== 0) {
    for (let i = 1; i <= rowCount; i++) {
        for (let j = 1; j <= rowCount - i; j++) {
            str += " ";
        }
        for (let k = 1; k <= (2 * i) - 1; k++) {
            str += k;
        }
        str += "\n";
    }
    console.log(str);
}
else {
    console.log("Number of columns is not an odd number.");
}
