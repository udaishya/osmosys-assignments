var n = window.prompt("Enter number of players(natural numbers only): ");
if (isNaN(n) === true || n<=0) {
    window.alert("Input is not a valid value. Use only natural numbers.");
}
else {
    var order = [];
    var str = [];
    for (let i = 0; i < n; i++) {
        var rank = Math.floor(Math.random() * n) + 1;
        if (order.includes(rank) === false) {
            order.push(rank);
        }
        else {
            i--;
        }
    }
    for (let i = 0; i < n; i++) {
        str += "Player " + (i+1) + " will bat at rank " + order[i] + "\n";
    }
    window.alert(str);
}
