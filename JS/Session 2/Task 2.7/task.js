var choice = window.prompt("Choose an option [1-4] to select which shape/'s area and perimeter is to be calculated: \n1. Square\n2. Triangle\n3. Circle\n4. Rectangle\n");
var str = "";
var area;
var perimeter;
if (choice < 1 || choice > 4) {
    window.alert("Invalid option. Input a value from 1 to 4.");
}
else {
    switch (parseInt(choice)) {
        case 1:
            let x = prompt("Enter the length of side of square: ");
            area = x * x;
            perimeter = 4 * x;
            str += "\nArea: " + area + " square units." + "\nPerimeter: " + perimeter + " units.";
            break;
        case 2:
            let a = prompt("Enter the length of one side of triangle: ");
            let b = prompt("Enter the length of second side of triangle: ");
            let c = prompt("Enter the length of third side of triangle: ");
            let s = (parseInt(a) + parseInt(b) + parseInt(c)) / 2;
            area = Math.sqrt(parseInt(s) * (parseInt(s) - parseInt(a)) * (parseInt(s) - parseInt(b)) * (parseInt(s) - parseInt(c)));
            perimeter = parseInt(a) + parseInt(b) + parseInt(c);
            str += "\nArea: " + area + " square units." + "\nPerimeter: " + perimeter + " units.";
            break;
        case 3:
            let r = prompt("Enter the radius of circle: ");
            area = Math.PI * r * r;
            perimeter = 2 * Math.PI * r;
            str += "\nArea: " + area + " square units." + "\nPerimeter: " + perimeter + " units.";
            break;
        case 4:
            let l = prompt("Enter the length of rectangle: ");
            let w = prompt("Enter the width of rectangle: ");
            area = l * w;
            perimeter = 2 * (parseInt(l) + parseInt(w));
            str += "\nArea: " + area + " square units." + "\nPerimeter: " + perimeter + " units.";
            break;
    }
    window.alert(str);
}
