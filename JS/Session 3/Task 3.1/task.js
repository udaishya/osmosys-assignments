let string = 'abbabcbdbabdbdbabababcbcbabcharFreq';
let arr = string.split("");
let result = new Map();
for(let i = 0;i < arr.length;i++){
    if(result.has(arr[i])){
        let size = result.get(arr[i])+1;
        result.set(arr[i],size);
    }
    else{
        result.set(arr[i],1);
    }
}
console.log(result);
