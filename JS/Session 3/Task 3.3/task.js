let arrayIn = prompt("Enter the array of numbers");
let array = arrayIn.split(",");
let result = new Map();
for(let i = 0;i < array.length;i++) {
    if (isNaN(array[i]) !== true) {
        if (result.has(array[i])) {
            let size = result.get(array[i]) + 1;
            result.set(array[i],size);
        }
        else{
            result.set(array[i],1);
        }
    }
}
let max = 0;
let ans;
let resultArray = [];
for(let [key,value] of result) {
    if(value >= max){
        max = value;
        ans = key;
    }
}
console.log("The number " + ans + " appears " + max +" more times than any other element in the array.");
