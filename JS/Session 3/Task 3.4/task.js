var str = "";
function uniqueCombination(l, sum, check, combArr, A) {
    if (sum == check) {
        for (let i = 0; i < combArr.length; i++) {
            if (i != 0)
                str += " ";
            str += combArr[i];
            if (i != combArr.length - 1)
                str += ", ";
        }
        str += "\n";
        return;
    }
 
    for (let i = l; i < A.length; i++) {
        if (sum + A[i] > check)
            continue;
        if (i > l && A[i] == A[i - 1])
            continue;
        combArr.push(A[i]);
        uniqueCombination(i + 1, sum + A[i], check, combArr, A);
        combArr.pop();
    }
}

function combination(A, check) {
    A.sort((a, b) => a - b);
    let combArr = [];
    uniqueCombination(0, 0, check, combArr, A);
}

let arrIn = window.prompt("Enter CSV of array elements: ");
let A = Array.from(arrIn.split(","), x => parseInt(x));
let check = parseInt(prompt("Enter the value of sum for the combination: "));
combination(A, check);
console.log(str);
