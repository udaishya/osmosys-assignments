let arrayBooks = [];
let arrayAuthors = [];

let addBooks = function(name,authorId,year,rating){
    let book = new Object();
    book.name = name;
    book.authorId = authorId;
    book.year = year;
    book.rating = rating;
    book.id = arrayBooks.length + 1;
    arrayBooks.push(book);
}
let addAuthors = function(name_author){
    let author = new Object();
    author.name = name_author;
    author.id = arrayAuthors.length + 1;
    arrayAuthors.push(author);
}

addBooks("Name of the wind",2,2015,4.5);
addBooks("Harry Potter",1,2000,4.0);
addBooks("Ham on Rye",3,1982,4.1);

addAuthors("JK Rowling");
addAuthors("Patrick Rothfuss");
addAuthors("Charles Bukowski");

let books_author = function(name){
    let authId = arrayAuthors.filter(aId => aId.name = name);
    authId = authId[0]["id"];
    let books = arrayBooks.filter(a => a.authorId == authId);
    for(let i = 0;i < books.length;i++){
        console.log(books[i]['name']);
    }
}

let book_rating = function(rating){
    let books = arrayBooks.filter(a => a.rating >= rating);
    for(let i = 0;i < books.length;i++){
        console.log(books[i]['name']);
    }
}

let edit_books = function(bookId,bookNewName){
    let book = arrayBooks.filter(a => a.id == bookId);
    book[0]['name'] = bookNewName;
}
let edit_author = function(authId,authorNewName){
    let author = arrayAuthors.filter(a => a.id == authId);
    author[0]['name'] = authorNewName;
}
