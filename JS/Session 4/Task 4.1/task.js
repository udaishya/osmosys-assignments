let people = {
    nameArray: [],
    birthdayArray: [],
    nameNextMonth: [],
    birthdayNextMonth: [],
    nameSunday: [],
    birthdaySunday: [],
    nameSameDay: [],
    birthdaySameDay: [],
    nameRemaining: [],
    birthdayRemaining: [],
    nameOldest: []
};

let addEntry = function (name,bDay) {
    let person = new Object();
    let birthD = new Object();
    person.name = name;
    people.nameArray.push(person);
    localStorage.setItem("names", JSON.stringify(people.nameArray));
    let temp = bDay.match(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
    birthD.birthday = new Date(temp[3],temp[2]-1,temp[1]);
    people.birthdayArray.push(birthD);
    localStorage.setItem("birthdays", JSON.stringify(people.birthdayArray));
}

let nextMonthBirthdays = function (month) {
    let m = parseInt(month)-1;
    let birthdays = localStorage.getItem("birthdays");
    let birthMonthArray = birthdays.match(/(?<=-)(\d{1,2})(?=-)/g);
    for (let i = 0; i < people.birthdayArray.length; i++) {
        if (birthMonthArray[i] == m) {
            people.birthdayNextMonth.push(people.birthdayArray[i]);
            people.nameNextMonth.push(people.nameArray[i]);
        }
    }
    localStorage.setItem("birthdayNextMonth", JSON.stringify(people.birthdayNextMonth));
    localStorage.setItem("nameNextMonth", JSON.stringify(people.nameNextMonth));
}

let sundayBirthdayThisYear = function (year) {
    let y = parseInt(year);
    let birthdays = JSON.parse(localStorage.getItem("birthdays"));
    let dob = [];
    let dobThisYear = [];
    for (let i = 0; i < birthdays.length; i++) {
        dob[i] = new Date(birthdays[i]["birthday"]);
        dobThisYear[i] = new Date(birthdays[i]["birthday"]);
        dobThisYear[i].setFullYear(y);
    }
    for (let i = 0; i < people.birthdayArray.length; i++) {
        if (dobThisYear[i].getDay() == 0) {
            people.nameSunday.push(people.nameArray[i]);
            people.birthdaySunday.push(dobThisYear[i]);
        }
    }
    localStorage.setItem("nameSunday", JSON.stringify(people.nameSunday));
    localStorage.setItem("birthdaySunday", JSON.stringify(people.birthdaySunday));
}

let sameDayBirthday = function () {
    let birthdays = JSON.parse(localStorage.getItem("birthdays"));
    let dob =[];
    for (let i = 0; i < birthdays.length; i++) {
        dob[i] = new Date(birthdays[i]["birthday"]);
    }
    for (let i = 0; i < people.birthdayArray.length; i++) {
        for (let j = 0; j < people.birthdayArray.length; j++) {
                if (dob[i].getDate() == dob[j].getDate() && dob[i].getMonth() == dob[j].getMonth() && i !== j) {
                    people.nameSameDay.push(people.nameArray[i]);
                    people.nameSameDay.push(people.nameArray[j]);
                    people.birthdaySameDay.push(people.birthdayArray[i]);
                    people.birthdaySameDay.push(people.birthdayArray[j]);
                }
        }
    }
    localStorage.setItem("nameSameDay", JSON.stringify(people.nameSameDay));
    localStorage.setItem("birthdaySameDay", JSON.stringify(people.birthdaySameDay));
}

let daysRemaining = function () {
    let birthdays = JSON.parse(localStorage.getItem("birthdays"));
    let dob = [];
    let dobThisYear = [];
    let year = (new Date()).getFullYear();
    let month = (new Date()).getMonth();
    let day = (new Date()).getDate();
    let today = new Date();
    let daysLeft = [];
    for (let i = 0; i < birthdays.length; i++) {
        dob[i] = new Date(birthdays[i]["birthday"]);
        dobThisYear[i] = new Date(birthdays[i]["birthday"]);
        dobThisYear[i].setFullYear(year);
    }
    for (let i =0; i < people.birthdayArray.length; i++) {
        if (dobThisYear[i].getMonth() > month || ((dobThisYear[i].getDate() > day) && (dobThisYear[i].getMonth() == month))) {
            daysLeft[i] = dobThisYear[i] - today;
            daysLeft[i] /= 86400000;
        }
        else if (dobThisYear[i].getMonth() <= month && dobThisYear[i].getDate() <= day){
            daysLeft[i] = today - dobThisYear[i];
            daysLeft[i] /= 86400000;
        }
    }
    for (let i = 0; i < people.birthdayArray.length; i++) {
        people.nameRemaining.push(people.nameArray[i]);
        people.birthdayRemaining.push(daysLeft[i]);
    }
    localStorage.setItem("nameRemaining", JSON.stringify(people.nameRemaining));
    localStorage.setItem("birthdayRemaing", JSON.stringify(people.birthdayRemaining));
}

let oldestPerson = function () {
    let birthdays = JSON.parse(localStorage.getItem("birthdays"));
    let dob = [];
    let age = [];
    for (let i = 0; i < birthdays.length; i++) {
        dob[i] = new Date(birthdays[i]["birthday"]);
        age[i] = (new Date() - dob[i])/(1000*3600*24);
    }
    let j = age.indexOf(Math.max(...age));
    people.nameOldest.push(people.nameArray[j]);
    localStorage.setItem("nameOldest", JSON.stringify(people.nameOldest));
}
