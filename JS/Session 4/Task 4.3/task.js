var inp = [];
inp = prompt("Enter the date of birth(m-d-Y): ");
inp = inp.match(/(\d{1,2})-(\d{1,2})-(\d{4})/);
inp = new Date(inp[3],inp[1]-1,inp[2]);
timer = prompt("Enter number of seconds for how long program should display new values: ");
for (let i = 0; i < parseInt(timer); i++) {
    setInterval((setTimeout(timeAlive, 0)), 1000);
}
function timeAlive() {
    let str = "";
    let d = new Date();
    str += ((d - inp)/1000) + " seconds.\n";
    str += ((d - inp)/(1000*3600)) + " hours.\n";
    str += ((d - inp)/(1000*3600*24)) + " days.\n";
    str += ((d - inp)/(1000*3600*24*7)) + " weeks.\n";
    str += ((d - inp)/(1000*3600*24*30)) + " months.\n";
    alert(str);
}
