var images = {
    "crow" : "img/crow.png",
    "tree" : "img/tree.png",
    "flower" : "img/flower.png",
    "cloud" : "img/cloud.png",
    "square" : "img/square.png",
    "triangle" : "img/triangle.png",
    "rectangle" : "img/rectangle.png",
    "circle" : "img/circle.png"  
  }  
  function populate() {
    if (quiz.isEnded()) {
      showScores();
    } else {
      // show question
      var element = document.getElementById("question");
      element.innerHTML = quiz.getQuestionIndex().text;
  
      // show options
      var choices = quiz.getQuestionIndex().choices;
      for (var i = 0; i < choices.length; i++) {
        var element = document.getElementById("choice" + i);
        element.innerHTML = images[choices[i]]? '<img src="'+images[choices[i]]+'"/>':choices[i];
        guess("btn" + i, choices[i]);
      }
    }
  };
  /*
  //random question (not working)
  function populate() {
    if (quiz.isEnded()) {
      showScores();
    } else {
      // show question
      var element = document.getElementById("question");
      element.innerHTML = quiz.getQuestionIndex().text;
  
      // show options
      var choices = quiz.getQuestionIndex().choices;
      var randomArray = [];
      var randomChoice = Math.random() * (choices.length - 0) + 0;
      randomArray.push(randomChoice);
      var choiceCount = 0;
      for (var i = 0; choiceCount < choices.length; i++) {
        if (randomArray.includes(randomChoice)) {
          randomChoice = Math.random() * (choices.length - 0) + 0;
          randomArray.push(randomChoice);
        }
        else {
          var element = document.getElementById("choice" + randomChoice);
          element.innerHTML = images[choices[randomChoice]]? '<img src="'+images[choices[randomChoice]]+'"/>':choices[randomChoice];
          guess("btn" + randomChoice, choices[randomChoice]);
          choiceCount++;
        }
      }
    }
  };
  */
  
  function guess(id, guess) {
    var button = document.getElementById(id);
    button.onclick = function() {
      quiz.guess(guess);
      populate();
    }
  };
  
  function showScores() {
    var gameOverHTML = "<h1>Result</h1>";
    gameOverHTML += "<h2 id='score'> Your scores: " + quiz.score + "</h2>";
    var element = document.getElementById("quiz");
    element.innerHTML = gameOverHTML;
  };
  
  // create questions
  var questions = [
    new Question("Click on the crow", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "crow"),
    new Question("Click on the tree", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "tree"),
    new Question("Click on the flower plant", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "flower"),
    new Question("Click on the cloud", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "cloud"),
    new Question("Click on the square", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "square"),
    new Question("Click on the triangle", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "triangle"),
    new Question("Click on the rectangle", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "rectangle"),
    new Question("Click on the circle", ["crow", "tree", "flower", "cloud", "square", "triangle", "rectangle", "circle"], "circle")
  ];
  
  function Question(text, choices, answer) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
  }
  
  Question.prototype.isCorrectAnswer = function(choice) {
    return this.answer === choice;
  }
  
  
  function Quiz(questions) {
    this.score = 0;
    this.questions = questions;
    this.questionIndex = 0;
  }
  
  Quiz.prototype.getQuestionIndex = function() {
    return this.questions[this.questionIndex];
  }
  
  Quiz.prototype.guess = function(answer) {
    if (this.getQuestionIndex().isCorrectAnswer(answer)) {
      this.score++;
      this.questionIndex++;
      alert("You selected the correct option");
    }
    else {
      alert("Incorrect! Try Again");
    }
  }
  
  Quiz.prototype.isEnded = function() {
    return this.questionIndex === this.questions.length;
  }
  
  // create quiz
  var quiz = new Quiz(questions);
  
  // display quiz
  populate();